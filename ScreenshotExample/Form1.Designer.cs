﻿namespace ScreenshotExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControls = new System.Windows.Forms.Panel();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.btnScreenshot = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picScreenshot = new System.Windows.Forms.PictureBox();
            this.panelControls.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picScreenshot)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControls
            // 
            this.panelControls.Controls.Add(this.txtUrl);
            this.panelControls.Controls.Add(this.btnScreenshot);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControls.Location = new System.Drawing.Point(0, 0);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(1498, 40);
            this.panelControls.TabIndex = 0;
            // 
            // txtUrl
            // 
            this.txtUrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrl.Location = new System.Drawing.Point(0, 0);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(1284, 35);
            this.txtUrl.TabIndex = 2;
            this.txtUrl.Text = "http://www.google.de";
            // 
            // btnScreenshot
            // 
            this.btnScreenshot.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnScreenshot.Location = new System.Drawing.Point(1284, 0);
            this.btnScreenshot.Name = "btnScreenshot";
            this.btnScreenshot.Size = new System.Drawing.Size(214, 40);
            this.btnScreenshot.TabIndex = 1;
            this.btnScreenshot.Text = "Get Screenshot";
            this.btnScreenshot.UseVisualStyleBackColor = true;
            this.btnScreenshot.Click += new System.EventHandler(this.btnScreenshot_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.picScreenshot);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1498, 1027);
            this.panel1.TabIndex = 1;
            // 
            // picScreenshot
            // 
            this.picScreenshot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picScreenshot.Location = new System.Drawing.Point(0, 0);
            this.picScreenshot.Name = "picScreenshot";
            this.picScreenshot.Size = new System.Drawing.Size(1498, 1027);
            this.picScreenshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picScreenshot.TabIndex = 2;
            this.picScreenshot.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1498, 1067);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelControls);
            this.Name = "Form1";
            this.Text = "BrowseEmAll Core API Screenshot Example";
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picScreenshot)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelControls;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Button btnScreenshot;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picScreenshot;
    }
}

