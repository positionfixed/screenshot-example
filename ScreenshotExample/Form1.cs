﻿using BrowseEmAll.API;
using BrowseEmAll.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScreenshotExample
{
    public partial class Form1 : Form
    {
        private BrowserManager manager = new BrowserManager(IntegrationMode.Standalone);
        private BrowserSettings settings = new BrowserSettings();

        public Form1()
        {
            InitializeComponent();

            manager.OnBrowserReady += Manager_OnBrowserReady;
            manager.OnDocumentComplete += Manager_OnDocumentComplete;
            manager.OnScreenshotCompleted += Manager_OnScreenshotCompleted;
        }

        private void btnScreenshot_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUrl.Text))
            {
                MessageBox.Show("Please enter a URL");
                return;
            }

            btnScreenshot.Enabled = false;

            // You could also use Browser.CHROME48 etc...
            manager.OpenBrowser(Browser.FIREFOX44, settings);
        }

        private void Manager_OnBrowserReady(object sender, BrowseEmAll.API.Events.BrowserReadyEventArgs e)
        {
            // Browser is ready to take commands
            manager.Navigate(e.BrowserID, txtUrl.Text);
        }

        private void Manager_OnDocumentComplete(object sender, BrowseEmAll.API.Events.DocumentCompletedEventArgs e)
        {
            // Browser has finished loading
            manager.GetScreenshot(e.BrowserID);
        }

        private void Manager_OnScreenshotCompleted(object sender, BrowseEmAll.API.Events.ScreenshotCompletedEventArgs e)
        {
            // Browser has taken a screenshot
            btnScreenshot.Enabled = true;

            using (var ms = new System.IO.MemoryStream(e.Screenshot))
            {
                var img = Image.FromStream(ms);
                picScreenshot.Image = img;
            }

            manager.CloseBrowser(e.BrowserID);
        }
    }
}
